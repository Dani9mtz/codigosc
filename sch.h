struct Actividad{
 char *titulo;
 char  *descripcion;
};


void act_introducirT(struct Actividad, char*);
void act_introducirD(struct Actividad, char*);

char* act_getTitulo(struct Actividad);
char* act_getDescripcion(struct Actividad);

void act_crearact(struct Actividad, char*, char*);
void act_ejecutaract(struct Actividad);
